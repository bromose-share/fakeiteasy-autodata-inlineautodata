﻿using AutoFixture;
using AutoFixture.AutoFakeItEasy;
using AutoFixture.Xunit2;
using Xunit;

namespace Bromose.FakeItEasy.Attributes
{
	public class AutoFakeItEasyDataAttribute : AutoDataAttribute
	{
		public AutoFakeItEasyDataAttribute()
			: base(() => new Fixture().Customize(new AutoFakeItEasyCustomization()))
		{
		}
	}


	public class InlineAutoFakeItEasyDataAttribute : CompositeDataAttribute
	{
		public InlineAutoFakeItEasyDataAttribute(params object[] values) : base(new InlineDataAttribute(values),
			new AutoFakeItEasyDataAttribute())
		{
		}
	}
}